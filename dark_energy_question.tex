\documentclass[a4paper,twoside, 11pt]{article}

% Packages
% ---------------------

\usepackage{amsmath} % Needed for command eqref
\usepackage{amssymb} % For math symbols like R \mathbb{R}
\usepackage{siunitx} % Physical numbers and units
\usepackage{hyperref} % Uses automatic references \autoref and urls

% Page settings
% ---------------------

\usepackage[top=2cm, bottom=2.5cm,left=2.5cm,right=2.5cm]{geometry} %  Page margins
\setlength{\parskip}{\baselineskip} % Add space between paragraphs
\parindent=0cm % Remove the paragraph indent of the first line
\addtolength{\jot}{2\jot} % Double the line between equations
\raggedbottom % Prevents spreading the page content vetically for non-full pages.

% Set vertical space around equations.
\AtBeginDocument{%
 \abovedisplayskip=15pt plus 3pt minus 9pt
 \abovedisplayshortskip=12pt plus 3pt
 \belowdisplayskip=20pt plus 3pt minus 9pt
 \belowdisplayshortskip=12pt plus 3pt minus 4pt
}

\begin{document}

\part*{Dark energy questions}

This is a solution to the dark energy questions asked in the video \url{https://www.youtube.com/watch?v=nZQIM1C6xQw}.

Document source: \url{https://bitbucket.org/evgenyneu/uni_2016_asp1010_dark_energy_question}


\subsection*{Initial data}

\begin{itemize}
  \item Current dark energy density: 68.3\%.
  \item Current regular matter and energy density: 31.7\% (includes both the “luminous matter” and dark matter).
  \item Amount of dark energy increases at the same rate as the volume of the universe. For example, if the Universe doubles in volume the dark energy double as well. If the Universe shrinks in volume by half dark energy also shrinks by half. (Aside: was there a law of energy conservation?)
\end{itemize}


\section*{Question 1}

\begin{quote}
For how many of the past 100 doublings has dark energy had any significant effect (at least 10\% of the energy density)?
\end{quote}

\subsection*{Hmm...}

Let $n$ be the number of times the Universe shrinks by half when we go to the past.

\begin{itemize}
  \item $n = 0$ means the present moment.
  \item $n = 1$ means we halve the universe once (linearly, not by volume, so the side of the Universe ``cube'' will be $0.5$ of the current length).
  \item $n = 2$ means we halve the universe twice (so the linear size will be $0.5*0.5 = 0.25$).
  \item $n = 3$ means we halve the universe three times ($0.5*0.5*0.5 = 0.125$).
\end{itemize}

\subsection*{Solution}

\begin{itemize}
  \item The \emph{linear size} of the Universe after we halve it $n$ times will be $0.5^n$ of the current size.
  \item The \emph{volume} will be $(0.5^n)^3 = 0.5^{3n}$.
  \item The amount of dark energy will be $0.5^{3n} \times 68.3$ units. Remember, the amount of dark energy is proportional to the volume.
  \item The amount of regular matter and energy after we halve the universe $n$ times will the same: $31.7$ units. Regular stuff does not change.
  \item The density of the dark energy equals
  \begin{align*}
    \textrm{Dark energy density} &= \frac{\textrm{[dark energy]}}{ \textrm{[dark energy]} + \textrm{[regular matter]}}\\
      &= \frac{0.5^{3n} \times 68.3}{ 0.5^{3n} \times 68.3 + 31.7}\\
      &= \frac{1}{ 1 + \frac{31.7}{0.5^{3n} \times 68.3}} \tag{Divide by $0.5^{3n} \times 68.3$}
  \end{align*}
  \item We can find the solution when dark energy is greater than 10\% by solving the inequality:
  \begin{align*}
    \frac{1}{ 1 + \frac{31.7}{0.5^{3n} \times 68.3}} & \geq 0.1\\
    1 + \frac{31.7}{0.5^{3n} \times 68.3} & \leq 10 \\
    \frac{31.7}{0.5^{3n} \times 68.3} & \leq 9 \\
    0.5^{3n} \times 68.3 & \geq \frac{31.7}{9} \\
    0.5^{3n} \times 68.3 & \geq 3.5\bar{2} \\
    0.5^{3n} & \geq \frac{3.5\bar{2}}{68.3} \\
    \log_{0.5} 0.5^{3n} & \leq \log_{0.5} {\Big(\frac{3.5\bar{2}}{68.3}\Big)} \tag{Take $\log_{0.5} $ of both sides} \\
    3n & \leq \log_{0.5} {\Big(\frac{3.5\bar{2}}{68.3}\Big)} \tag{Use the property $\log_a {a^b} = b$}\\
    n & \leq \log_{0.5} {\Big(\frac{3.5\bar{2}}{68.3}\Big)} / 3 \\
    n & \leq \num{1.425776}. \tag{Calculate approximately}\\
  \end{align*}
\end{itemize}

\subsection*{Answer to the question 1}

Dark energy had significant effect during the last \underline{$\num{1.425776}$ doublings} of the Universe size.


\section*{Extra credit question 1}

\begin{quote}
How many billion years ago dark energy started to have significant effect (at least 10\% of the energy density)?
\end{quote}

If we halve the Universe $\num{1.425776}$ times to the past it will have the size (am I using the right grammar here to describe our future action when we reverse the flow of time and go to the past?)
\[
  \textrm{Past size} = 0.5^{1.425776} = \num{0.372219}.
\]
The value $\num{0.372219}$ means it will be $\num{0.372219}$ times smaller in linear size than the current Universe. If we assume that the universe expands at constant rate than we simply multiply the current age of the Universe ($14$ billion years) by $\num{0.372219}$ to get the age when it was that small
\[
  \textrm{Age} = \SI{13.772e9}{year} \cdot \num{0.372219} = \SI{5.126e9}{year}.
\]

\subsection*{Answer to the extra credit question 1}

The Universe was about $5.126$ billion years old when dark matter started to have significant effect.





\section*{Question 2}

\begin{quote}
For how many of the future doublings will regular matter and energy have a significant effect (at least 10\% of the energy density)?
\end{quote}

\subsection*{Huh?}

Let $n$ be the number of times the Universe doubles in linear size.

\begin{itemize}
  \item $n = 0$ means the present moment.
  \item $n = 1$ means we double the universe once (the side of the Universe ``cube'' will be $2$ of the current length).
  \item $n = 2$ means we double the universe twice (so the side of the ``cube'' will be $2*2 = 4$).
  \item $n = 3$ means we double the universe three times ($2*2*2 = 8$).
\end{itemize}


\subsection*{Solution}

\begin{itemize}
  \item The \emph{linear size} of the Universe after we double it $n$ times will be $2^n$ of the current size.
  \item The \emph{volume} will be $(2^n)^3 = 2^{3n}$.
  \item The amount of dark energy will be $2^{3n} \times 68.3$ units.
  \item The amount of regular matter and energy after we double the universe $n$ times will the same: $31.7$ units.
  \item The density of the regular matter and energy equals
  \begin{align*}
    \textrm{Regular matter/energy density} &= \frac{\textrm{[regular matter]}}{ \textrm{[dark energy]} + \textrm{[regular matter]}}\\
      &= \frac{31.7}{ 2^{3n} \times 68.3 + 31.7}.
  \end{align*}
  \item We find the solution when the regular matter/energy is greater than 10\% by solving the inequality:
  \begin{align*}
    \frac{31.7}{ 2^{3n} \times 68.3 + 31.7} & \geq 0.1\\
    2^{3n} \times 68.3 + 31.7 & \leq 317 \\
    2^{3n} \times 68.3 & \leq 285.3 \\
    2^{3n}  & \leq \frac{285.3}{68.3}\\
    \log_{2} 2^{3n} & \leq \log_{2} {\Big(\frac{285.3}{68.3}\Big)} \tag{Take $\log_{2} $ of both sides} \\
    3n & \leq \log_{2} {\Big(\frac{285.3}{68.3}\Big)} \tag{Use the property $\log_a {a^b} = b$}\\
    n & \leq \log_{2} {\Big(\frac{285.3}{68.3}\Big)} / 3 \\
    n & \leq \num{0.687507}. \tag{Calculate approximately}\\
  \end{align*}
\end{itemize}

\subsection*{Answer to the question 2}

Regular matter and energy will continue to have a significant effect for next \underline{$\num{0.687507}$ doublings} of the Universe size.



\section*{Extra credit question 2}

\begin{quote}
How many billion years in the future will regular matter/energy cease to matter (less than 10\% of the energy density)?
\end{quote}

If we double the Universe $\num{0.687507}$ times it will have the size
\[
  \textrm{Future size} = 2^{0.687507} = \num{1.610498}.
\]
The value $\num{1.610498}$ means the future Universe will be $\num{1.610498}$ larger  in linear size than the current Universe. If we assume that the universe expands at constant rate than we simply multiply the current age of the universe ($14$ billion years) by $\num{1.610498}$ to get the age by which the Universe will have expanded
\[
  \textrm{Age} = \SI{13.772e9}{year} \cdot \num{1.610498} = \SI{22.2e9}{year}.
\]
Finally, we calculate the difference between the $\SI{22.2e9}{years}$ and the current age
\[
  \SI{22.2e9}{year} - \SI{13.772e9}{year} = \SI{8.428e9}{year}.
\]

\subsection*{Answer to the extra credit question 2}

The regular matter/energy will cease to matter in $\num{8.428}$ billion years.

\end{document}